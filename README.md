# Databox integration

To get things started quickly I chose lumen as a micro-framework.

## Installation

First you need to check the project from GitLab with git client: 

```bash
git clone git@gitlab.com:Kovacec/databox.git
```

Change directory:

```bash
cd databox
```

Install dependencies with composer. 

```bash
composer install
```

## License

Open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
